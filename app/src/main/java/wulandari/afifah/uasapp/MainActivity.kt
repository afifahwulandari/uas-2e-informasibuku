package wulandari.afifah.uasapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.mainmenu.*

class MainActivity : AppCompatActivity() , View.OnClickListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.mainmenu)


        btbook.setOnClickListener(this)
        kat.setOnClickListener(this)
        btset.setOnClickListener(this)


    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btbook->{

                val ina = Intent(this,book::class.java)
                startActivity(ina)
                 }
            R.id.kat->{
                val ina = Intent(this,kategori::class.java)
                startActivity(ina)

            }
            R.id.btset->{
                val ina = Intent(this,setting::class.java)
                startActivity(ina)

            }
        }
    }
}
