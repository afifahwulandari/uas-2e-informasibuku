package wulandari.afifah.uasapp
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.kategorilayout.*

class Adapter(val dataMhs: List<HashMap<String,String>>, val kat : kategori): RecyclerView.Adapter<Adapter.HolderDataMhs> (){
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): Adapter.HolderDataMhs {
        val v  = LayoutInflater.from(parent.context).inflate(R.layout.row_jenis,parent,false)
        return HolderDataMhs(v)
    }

    override fun getItemCount(): Int {
        return dataMhs.size
    }

    override fun onBindViewHolder(holder: Adapter.HolderDataMhs, position: Int) {
        val data =dataMhs.get(position)
        holder.nm_jen.setText(data.get("nm_jenis"))
        if (position.rem(2) == 0) holder.layouts.setBackgroundColor(Color.rgb(230,245,240))
        else holder.layouts.setBackgroundColor(Color.rgb(255,255,245))

        holder.layouts.setOnClickListener(View.OnClickListener{
            kat.id_kate = data.get("id_jenis").toString()
            kat.jnsBuku.setText(data.get("nm_jenis"))
          /*  val pos = mainActivity.daftarprodi.indexOf(data.get("nama_prodi"))
            mainActivity.SpProdi.setSelection(pos)
            mainActivity.txNm.setText(data.get("nim"))
            mainActivity.txNamaMhs.setText(data.get("nama"))
            Picasso.get().load(data.get("url")).into(mainActivity.imageView2)

           */
        })


    }

    class HolderDataMhs(v : View) :  RecyclerView.ViewHolder(v){
        val nm_jen = v.findViewById<TextView>(R.id.jnsBuku)
         val layouts = v.findViewById<ConstraintLayout>(R.id.jenis)
    }

}