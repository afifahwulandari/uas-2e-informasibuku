package wulandari.afifah.uasapp

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.splass.*


class SplassActivity : AppCompatActivity(){

    var warnakat = ""
    lateinit var pref : SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splass)

        pref = getSharedPreferences("a", Context.MODE_PRIVATE)
        warnakat = pref.getString("bl","").toString()
        warna()

        val handler = Handler()
        handler.postDelayed({
            val intent = Intent(this,login::class.java)
            startActivity(intent)
        },5000)

    }

    fun warna(){

        if (warnakat=="kuning"){
            laysplass.setBackgroundColor(Color.YELLOW)
        }else if(warnakat=="biru"){
            laysplass.setBackgroundColor(Color.BLUE)
        }else if (warnakat=="putih"){
            laysplass.setBackgroundColor(Color.WHITE)
            txHeader.setTextColor(Color.BLACK)
            txby.setTextColor(Color.BLACK)
        }else if(warnakat=="hijau"){
            laysplass.setBackgroundColor(Color.GREEN)
        }

    }

}