package wulandari.afifah.uasapp

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*

class AdapterBuku(val datBook: List<HashMap<String,String>>, val book :book): RecyclerView.Adapter<AdapterBuku.HolderDataMhs> (){
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterBuku.HolderDataMhs {
        val v  = LayoutInflater.from(parent.context).inflate(R.layout.row_buku,parent,false)
        return HolderDataMhs(v)
    }

    override fun getItemCount(): Int {
        return datBook.size
    }

    override fun onBindViewHolder(holder: AdapterBuku.HolderDataMhs, position: Int) {
        val data =datBook.get(position)
        holder.idbuku.setText(data.get("id_buku"))
        holder.jdl.setText(data.get("judul_buku"))
        holder.nm_peng.setText(data.get("nm_pengarang"))
        holder.harga.setText(data.get("harga"))
        holder.nm_jen.setText(data.get("nm_jenis"))
        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(holder.pict)
        if (position.rem(2) == 0) holder.layouts.setBackgroundColor(Color.rgb(255,255,245))

        holder.layouts.setOnClickListener(View.OnClickListener{
            book.txID.setText(data.get("id_buku"))
            book.txJDL.setText(data.get("judul_buku"))
            book.pengarang.setText(data.get("nm_pengarang"))
            book.harga.setText(data.get("harga"))
            val pos = book.daftarmerk.indexOf(data.get("nm_jenis"))
            book.SpProdi.setSelection(pos)
            Picasso.get().load(data.get("url")).into(book.imageView2)

            //kat.id_kate = data.get("id_jenis").toString()
            //kat.jnsBuku.setText(data.get("nm_jenis"))
            /*  val pos = mainActivity.daftarprodi.indexOf(data.get("nama_prodi"))
              mainActivity.SpProdi.setSelection(pos)
              mainActivity.txNm.setText(data.get("nim"))
              mainActivity.txNamaMhs.setText(data.get("nama"))
              Picasso.get().load(data.get("url")).into(mainActivity.imageView2)

             */
        })


    }

    class HolderDataMhs(v : View) :  RecyclerView.ViewHolder(v){
        val idbuku = v.findViewById<TextView>(R.id.idBuku)
        val jdl = v.findViewById<TextView>(R.id.jdlBuku)
        val nm_peng = v.findViewById<TextView>(R.id.nmPeng)
        val nm_jen = v.findViewById<TextView>(R.id.jnsBuku)
        val  harga = v.findViewById<TextView>(R.id.hrg)
        val pict = v.findViewById<ImageView>(R.id.ImgUpload)
        val layouts = v.findViewById<ConstraintLayout>(R.id.aa)
    }

}